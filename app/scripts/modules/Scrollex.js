import { APP_NAME, isDebug } from '../utils/environment';
import AbstractModule from './AbstractModule';

const MODULE_NAME = 'Scrollex';
const EVENT_NAMESPACE = `${APP_NAME}.${MODULE_NAME}`;

const EVENT = {
    CLICK: `click.${EVENT_NAMESPACE}`
};

export default class extends AbstractModule {
    constructor(options) {
        super(options);

        // Declaration of properties
        if(isDebug) console.log('🔨 [module]:constructor - '+MODULE_NAME);
        this.type       = (this.$el.data('data-type') !== null) ? this.$el.data('data-type') : 'default';
        this.$elements  = this.$el.find('.js-visible');
    }

    init() {
        // Set events and such
        if(this.type === 'default'){
            this.$elements.scrollex({
                top: '-10%',
                bottom: '-10%',
                mode: 'middle',
                enter: function(){
                    $(this).addClass('u-visible');
                },
                leave: function(){
                    $(this).removeClass('u-visible');
                }
            });
        }
        else if(this.type === 'initial'){
            this.$elements.scrollex({
                top: '50%',
                enter: function(){
                    $(this).addClass('u-visible');
                }
            });
        }
    }

    destroy() {
        super.destroy(isDebug, MODULE_NAME, EVENT_NAMESPACE);
    }
}