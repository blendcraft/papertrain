<p align="center">
<a href="#"><img src="https://i.imgur.com/s4wcp4b.png"/></a>
<img style="display:inline-block;" src="https://img.shields.io/badge/markup-HTML5-orange.svg?style=flat-square"/>
<img style="display:inline-block;" src="https://img.shields.io/badge/style-CSS3-blue.svg?style=flat-square"/>
<img style="display:inline-block;" src="https://img.shields.io/badge/javascript-ES6-yellow.svg?style=flat-square"/>
<img style="display:inline-block;" src="https://img.shields.io/badge/CMS-Craft%20CMS%20>=3.0.0--R1-ff69b4.svg?style=flat-square"/>
<img style="display:inline-block;" src="https://img.shields.io/badge/dependencies-up%20to%20date-brightgreen.svg?style=flat-square"/>
<a style="display:inline-block;" href="https://github.com/AndrewK9/papertrain/blob/master/LICENSE"><img src="https://img.shields.io/badge/license-MIT-lightgray.svg?style=flat-square"/></a>
</p>

# Papertrain - A Craft CMS Boilerplate
Front-end boilerplate for [Craft 3](https://craftcms.com/3) based on the boilerplates by [Pageworks](https://page.works/) and [Locomotive](https://locomotive.ca/). Includes SASS snippets from [Intuit](https://github.com/intuit) and grid system concepts by [Harry Roberts](https://csswizardry.com/2011/08/building-better-grid-systems/).

## Requirements
| Prerequisite    | How to check  | How to install                   |
| --------------- | ------------- | -------------------------------- |
| Node.js 4.1.1   | `node -v`     | [nodejs.org](https://nodejs.org) |
| Grunt >= 0.1.13 | `grunt -v`    | `npm install -g grunt-cli`       |

## Getting Started

1. Clone this GitHub repo
1. Downloading the node moduels
    1. `cd` into your projects directory
    1. `npm install`
1. Downloading Craft CMS
    1. `composer install`
    1. Create a new directory `mkdir storage` 
1. Setting up the database
    1. Connect to your MySQL server via your preferred interface (ex. [Sequel Pro](https://www.sequelpro.com/))
    1. Create a new database `craft_example`
        1. Encoding: `utf8`
        1. Collation: `general_ci`
    1. Duplicate the `.env.example` file
    1. Enter the information
        1. Type anything into the security key
1. Setting up the server
    1. Launch your preferred server software (ex. [MAMP](https://www.mamp.info/en/))
    1. Add your new host `example.craft.local`
    1. Set the root folder to be your projects `public` folder
    1. Save your host settings
        * Restart may be required
1. Installing Craft
    1. In your preferred brower go to `exmaple.craft.local/admin`
        * If it doesn't work try `example.craft.local/index.php?p=admin`
    1. Follow the on-screen instructions
1. Setting up browser sync
    1. Open `build/grunt/gconfig/browserSync.js`
    1. Change the proxy to `exmaple.craft.local`

## CSS
- Using [SASS](https://sass-lang.com/) as our CSS preprocessor
- CSS architecture based on concepts by [Harry Roberts](https://csswizardry.com/2011/08/building-better-grid-systems/)
- Minimal BEM like CSS Syntax `.block_element -modifier`
- SASS snippets from [Intuit](https://github.com/intuit)
- Using [Transparent UI Namespaces](https://csswizardry.com/2015/03/more-transparent-ui-code-with-namespaces/)
- Uisng [Bourbon](https://www.bourbon.io/) for additonal mixins for SASS utility

### SASS Import
1. __Settings:__ global variables and colour pallets.
1. __Vendors:__ bourbon mixins for base utility `@include size(x, y)`
1. __Tools:__ site wide mixins.
1. __Generic:__ low level far reaching generic element settings.
1. __Base:__ css for unclassed HTML elements `p {}, blockquote {}, ul {}` go in page.
1. __Objects:__ generic objects and object elements `.o-youtube-video`.
1. __Components:__ complete secitons of UI `.c-dca -copy`.
1. __Templates:__ css for specific template pages, used to style individual and unique elements within a page.
1. __Utilities:__ generic utility selectors, used to adjust elements in a very specific way `.u-text-center`

### Grid
Uisng a inline-block gird system by [Intuit](https://github.com/intuit).

Start by inserting a `.o-layout` block and add a `.o-layout_item` element within the block. Initial blocks are `width: 100%`, additional values are avialable using `.u-1/2 .u-1/3 .u-1/4 ect...`. Additional fractions can be generated from `app/sass/tools/utilities/widths.scss`, currently using `1 2 3 4 5` values.

Create responsive elements using `@size` on the `.u-1/x` classes. If you wanted a block to start at full width but switch to 1/2 at a medium screen use `u-1/2@medium`. You can adjust the generated breakpoints in `app/sass/tools/utilities/widths.scss` by adding or removing `@include widths($widths-fractions, $size-variable-from-config, desired-@-name)`.

## JavaScript
- Uses [Locomotive's](https://locomotive.ca/) JavaScript modules structure. Add JavaScript modules via HTML data attributes: `data-module="example"`
- All DOM related JavaScript is hooked to `js-` prefixed classes
- [jQuery](https://jquery.com/) is globally included
- [scrollex](https://github.com/ajlkn/jquery.scrollex) is globally included
- [FontAwesome 5](https://fontawesome.com/) is globally included

## Page Transitions
Using [Pjax by MoOx](https://github.com/MoOx/pjax) and inital JavaScript framework from [Locomotive's](https://locomotive.ca/) boilerplate.

### Usage
1. `base.twig` includes the basic pjax container/wrapper setup. When a transition is launched the new container is put the pjax wrapper and the old one is removed.
1. Main settings are inside `app/scripts/transitions/TransitionManager.js`
1. `BaseTransition` is launched by default. To use a custom transition:
    - Create a new class `TestTransition.js` which extends `BaseTransition`
    - Add a line in `app/scripts/transitions/transitions.js` like `export {default as TestTransition} from './TestTransition'`
    - Usage: `<a href="" data-transition="TestTransition">My Link</a>`

### Schema

Initially created by [Locomotive](https://locomotive.ca/)

Legend
- `[]`: listener
- `*`: trigger event

`[pjax:send]` -> (transition) launch()

`[pajx:switch}` (= new view is loaded) -> (BaseTransition) hideView() -> hide animations & `*readyToRemove`

`[readyToRemove]` -> `remove()` -> delete modules & remove oldView from the DOM, innerHTML newView, init modules, `display()`

`display()` -> (BaseTransition) `displayView()` -> display animations & `*readyToDestroy` -> init new modules

`*readyToRemove` -> reinit()

## Contact
Kyle Andrews |
|--------------------------------------------|
| [Email](mailto:andrews@gamesbykyle.com)           |
| [GitHub](https://github.com/andrewk9)      |
| [Twitter](https://twitter.com/GamesByKyle) |

## Feedback
Feel free to [open an issue](https://github.com/AndrewK9/papertrain/issues).

## License
[MIT](https://github.com/AndrewK9/papertrain/blob/master/LICENSE)